package test;

agent Manager extends astra.protocol.FipaRequest, astra.protocol.FipaSubscribe {
    types manager {
        formula auction_count(int);
        formula bidder(string);
    }

    initial channel("new_auctions"), auction_count(0);
    initial !monitor_action_requests(), !monitor_subscriptions();
    initial !init();
    
    // ----------------------------------------------------------------------------------------------------
    // create_bidder(string name) rules
    // ----------------------------------------------------------------------------------------------------
    rule +!evaluate_request(string sender, create_bidder(string name), boolean res) : ~bidder(name) {
        res = true;
    }

    rule +!execute_request(string sender, create_bidder(string name), funct outcome, boolean result) {
        system.createAgent(name, "Bidder");
        outcome = created(name);
        result = true;
    }

    // ----------------------------------------------------------------------------------------------------
    // sell_item(string item, int qty) rules
    // ----------------------------------------------------------------------------------------------------
    rule +!evaluate_request(string sender, sell_item(string item, int qty), boolean res) {
        res = true;
    }

    rule +!execute_request(string sender, sell_item(string item, int qty), funct outcome, boolean result)  : auction_count(int count) {
        string name = "auctioneer_" + count;
        system.createAgent(name, "Auctioneer");
        -+auction_count(count+1);

        !publish("new_auctions", auction(item, name));
        
        system.sleep(2000);
        !request_action(name, start_auction(item, qty));

        outcome = selling(item, qty);
        result = true;
    }
}