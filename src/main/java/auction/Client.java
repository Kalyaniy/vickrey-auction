package auction;

import mams.utils.Identifier;

public class Client {
    @Identifier
    public String id;
    
    public String notificationUrl;

    public String bidderUrl;
}