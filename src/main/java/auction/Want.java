package auction;

import mams.utils.Identifier;

public class Want {
    @Identifier
    public String id;

    public String item;

    public int unitBid;

    public int quantity;
}